"""
Wrapper function to optimise the step() method of mkp_opt_env using numerical optimisers from scipy
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
from cpymad.madx import Madx

# Import the mkp environment
import mkp_opt_env

# Initiate parameters 
optics = 'q20'
save_plot = True
randomize_waveform_risetimes = True  # select whether to randomly modify the rise times of the MKP delays with a sigmoid function

# Start the environment depending on the randomization, and test it with different time shift vectors 
if randomize_waveform_risetimes:
    save_str = "_sigmoid"
    mkp_env = mkp_opt_env.MKPOptEnv(optics, randomize_waveforms=True) 
else:
    save_str = ""
    mkp_env = mkp_opt_env.MKPOptEnv(optics, randomize_waveforms=False) 

# Give some tests values for the time shifts, but general and the individual 
delta_t = 4970 #general time shift
mkp_env.dtc = 250  #time shift to circulating beam
delta_t_single = 0  #time shift for each waveform
dtau = np.full(8, delta_t_single)
dtau = np.append(dtau, delta_t)

# Perform a first reset observation
obs = mkp_env.reset()

# Carry out the optimisation
dtau0 = np.zeros(9)
dtau0[-1] = delta_t 
bounds = ((0, 50), (0, 50), (0, 50), (0, 50), (0, 50), (0, 50), (0, 50), (0, 50), (4800, 5600))
results = optimize.minimize(mkp_env.step_dkick, dtau0, bounds=bounds, method='Powell', options={'tol':1e-18}) 
print(results)
mkp_env.generate_twiss()

#Plot the injection envelope
fig1 = plt.figure(figsize=(10,7))
mkp_env.plot_oscillations(fig1)
if save_plot:
    fig1.savefig('plots/mkp_waveform_dt_{}_and_dtc_{}{}.png'.format(delta_t, mkp_env.dtc, save_str), dpi=250)

#Plot where the kicks occurs in the waveforms
fig2 = plt.figure(figsize=(10,7))
mkp_env.plot_mkp_kicks(fig2)
if save_plot:
    fig2.savefig('plots/waveforms_dtc_{}_ns{}.png'.format(mkp_env.dtc, save_str), dpi=250)

#Plot action-angle variables 
fig3 = plt.figure(figsize=(10,7))
mkp_env.plot_J(fig3)
plt.show()
if save_plot:
    fig3.savefig('plots/new_J_dtc_{}_ns{}.png'.format(mkp_env.dtc, save_str), dpi=250)


# Uncomment below if optimising for the emittance blow-p - not yet fully tested
"""
#Try out the optimisation for emittance
t_mkp_0 = 5100
bounds = (4800, 5600)
results_ex = optimize.minimize(mkp_env.step_ex, t_mkp_0, method='Powell') 
#results = optimize.minimize(mkp_env.step_dkick, dtau0, bounds=bounds, method='Nelder-Mead', options={'tol':1e-18, 'adaptive': True})
print("Best t_mkp with lowest total emittance: {:.2f} ns".format(results_ex.x[0]))

mkp_env.generate_twiss()
#"""  
