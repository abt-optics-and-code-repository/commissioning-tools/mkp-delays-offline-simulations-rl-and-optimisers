"""
Optimisation environment to find the ideal time shift vector dtau for the electrical switches 
of the MKPs in the SPS injection in order to minimise the horizontal beam oscillations in the SPS 
To use on stationary computer mounted on the CERN network. 
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cpymad.madx import Madx

#Import accelerator physics library with functions
sys.path.append('/eos/user/e/elwaagaa/documents/waagaard_tech_studentship/python/functions')
import acc_phy_lib_elias 

class MKPOptEnv():
    
    #Load optics, voltage-to-kick converted data, and corresponding time data
    def __init__(self, optics):
        self.optics = optics
        self.mkp_kick_data = np.genfromtxt('/eos/user/e/elwaagaa/SWAN_projects/elias_technical_studentship/nxcals/mkp_waveforms/example_data/sps_lhcpilot_kick_data.txt') 
        self.time_data = np.genfromtxt('/eos/user/e/elwaagaa/SWAN_projects/elias_technical_studentship/nxcals/mkp_waveforms/example_data/sps_lhcpilot_time_data.txt') 
        self.max_timestamp = 6004   #timestamp of flat top kick in ns,  use a third into the flat top for MKP module 16 (meaning exactly at the midpoint - 1/6*width)
        self.dtc = 1100  #time shift in ns between injected batch and ciruclating batch in the SPS
        
    #Method to calculate oscillations of the injected and circulating beam (c) for a given timeshift vector for the MKP switches 
    def step(self, dtau):
        #Here pipe in time shift vector, output average oscillations 
        
        #Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        t = self.max_timestamp + dtau[-1]  #new general time stamp 
        tc = self.max_timestamp + dtau[-1] - self.dtc  #new general time stamp for circulating beam
        self.t_inds = []
        self.tc_inds = []
        for i in range(8):  #8 switches in total to iterate over 
            t_ind = np.argwhere(self.time_data==(t+dtau[i] - ((t+dtau[i]) % 2)))  #find closest even index corresponding to this time 
            tc_ind = np.argwhere(self.time_data==(tc+dtau[i] - ((tc+dtau[i]) % 2)))  
            self.t_inds.append(t_ind)
            self.tc_inds.append(tc_ind)
        
        #Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i!=0):  #after every two steps, change electrical switch
                switch_count += 1  
            kick = self.mkp_kick_data[i][self.t_inds[switch_count]]
            kick_c = self.mkp_kick_data[i][self.tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)
        
        #Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])
        
        # ------------- INJECTED BEAM, UPDATE MKP KICKS ----------------------
        #Update the MKP kicks through the MADX globals
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpa.11931'].kick = self.mkp_kicks[0] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpa.11936'].kick = self.mkp_kicks[1] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpc.11952'].kick = self.mkp_kicks[2] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkp.11955'].kick = self.mkp_kicks[3] 
        
        # ------------- CIRCULATING BEAM, UPDATE MKP KICKS ----------------------
        #Update the MKP kicks through the MADX globals
        self.madx.sequence['tt2tt10sps'].elements['mkpa.11931'].kick = self.mkp_kicks_c[0] 
        self.madx.sequence['tt2tt10sps'].elements['mkpa.11936'].kick = self.mkp_kicks_c[1] 
        self.madx.sequence['tt2tt10sps'].elements['mkpc.11952'].kick = self.mkp_kicks_c[2] 
        self.madx.sequence['tt2tt10sps'].elements['mkp.11955'].kick = self.mkp_kicks_c[3] 
        self.madx.globals['kmdh12007'] = 0  #has to be turned off from the loaded sequence
        
        #Attempt Twiss commands for the injected and circulating beam , restart MADX if it crashes 
        try:      
            #Injected beam
            self.madx.use(sequence='short_injection_and_some_sps')
            twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                    bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                                   dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                    dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                    x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                    px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
    
            #Find the average amplitude of the oscillations for the injected beam
            x_bar = np.average(abs(twiss.loc['qf.12010':'qf.13010'].x))
            px_bar = np.average(abs(twiss.loc['qf.12010':'qf.13010'].px))
                    
            #Circulating beam
            self.madx.use(sequence="tt2tt10sps")
            twiss_c = self.madx.twiss(betx=self.initbeta0['betx'], alfx=self.initbeta0['alfx'],
                                bety=self.initbeta0['bety'], alfy=self.initbeta0['alfy'], 
                               dx=self.initbeta0['dx'], dpx=self.initbeta0['dpx'],
                                dy = self.initbeta0['dy'], dpy=self.initbeta0['dpy'],
                                x=self.initbeta0['x'], y=self.initbeta0['y'],
                                px=self.initbeta0['px'], py=self.initbeta0['py']).dframe()
            
            #Find the average amplitude of the oscillations for the circulating beam
            x_bar_c = np.average(abs(twiss_c.loc['qf.12010':'qf.13010'].x))
            px_bar_c = np.average(abs(twiss_c.loc['qf.12010':'qf.13010'].px))
            
            #Define objective function
            obj_func = x_bar**2 +  px_bar**2 + x_bar_c**2 + px_bar_c**2 
        
        #Restart MADX if it crashes
        except RuntimeError:
            print("Resetting MADX...") 
            with open('tempfile', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()
            #Set the objective function to a high value
            obj_func = 100        
        
        return obj_func
    
        
    #Method to reset madx if it crashes
    def reset(self): 
        #If MAD-X fails, re-spawn process   
        if 'self.madx' in globals():
            del self.madx
        
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f,stderr=f)        
        self.madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)
        
        #Load sequence from class method 
        self.load_seq()


    #Method to load injection sequence and parts of SPS 
    def load_seq(self):
       
        #Activate the aperture for the Twiss flag to include it in Twiss command! 
        self.madx.input('select,flag=twiss,clear;')
        self.madx.select(flag='twiss', column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2', 'aptol_1', 'aptol_2', 'aptol_3']) 
       
        # -------------------------- FIRST LOAD CIRCULATING BEAM ---------------------------------------
        #Load sequence and initial conditions for tt2tt10SPS
        self.madx.call('/eos/user/e/elwaagaa/SWAN_projects/elias_technical_studentship/sps_sequences_and_twiss_ic/tt2tt10sps_{}.seq'.format(self.optics))
        self.madx.input('option, RBARC=FALSE;')
        self.initbeta = pd.read_csv('/eos/user/e/elwaagaa/SWAN_projects/elias_technical_studentship/sps_sequences_and_twiss_ic/initbeta0_whole_sps_{}.csv'.format(self.optics), index_col=0)
        self.initbeta0 = self.initbeta.iloc[0]
    
        #Remove the change of reference matrix
        self.madx.command.seqedit(sequence='tt2tt10sps')
        self.madx.command.flatten()
        self.madx.command.remove(element='change_ref')
        self.madx.command.endedit()                     
        
        # -------------------------- THEN LOAD INJECTED BEAM, WITH SOME SPS ------------------------------
        #Load sequence and initial conditions for short_injection_and_some_sps, to get the injected beam and about 412 m into the SPS 
        self.madx.call('/eos/user/e/elwaagaa/SWAN_projects/elias_technical_studentship/sps_sequences_and_twiss_ic/short_injection_and_some_sps_{}.seq'.format(self.optics))
        #Load Twiss and survey parameters
        self.twiss_reversed = pd.read_csv("/eos/user/e/elwaagaa/SWAN_projects/elias_technical_studentship/sps_sequences_and_twiss_ic/sps_injection_twiss_reversed_{}.csv".format(self.optics))
        self.twiss_beta1 = self.twiss_reversed.iloc[-1] #locate Twiss parameters at the start of the loaded reversed sequence, initial conditions for later
        
        #Extract beam info - same for injected and circulating 
        self.madx.use(sequence='short_injection_and_some_sps')
        self.sige = self.madx.sequence['short_injection_and_some_sps'].beam['sige']  #relative energy spread
        self.ex = self.madx.sequence['short_injection_and_some_sps'].beam['ex']
        self.ey = self.madx.sequence['short_injection_and_some_sps'].beam['ey']
        
        #Add the mechanical apertures 
        self.madx.input('qif.103000, APERTYPE=ELLIPSE, APERTURE={0.076,0.01915};')
        self.madx.input('qda.11910, APERTYPE=ELLIPSE, APERTURE={0.0755,0.0455};')
        self.madx.input('tbsj.11995, APERTYPE=RECTANGLE, APERTURE={0.0608,0.016};')
        self.madx.input('qf.12010 , APERTYPE=ELLIPSE, APERTURE={0.076,0.01915};')
        self.madx.input('mkpa.11931 , APERTYPE=RECTANGLE, APERTURE={0.05,0.0305};')
        self.madx.input('mkpa.11936 , APERTYPE=RECTANGLE, APERTURE={0.05,0.0305};')
        self.madx.input('mkpc.11952 , APERTYPE=RECTANGLE, APERTURE={0.05,0.0305};')
        self.madx.input('mkp.11955 , APERTYPE=RECTANGLE, APERTURE={0.07,0.027};')
        self.madx.input('mdsh.11971 , APERTYPE=ELLIPSE, APERTURE={0.06,0.027};')
        self.madx.input('loe.12002, APERTYPE=ELLIPSE, APERTURE={0.076,0.01915};')
        self.madx.input('lsf.12005, APERTYPE=ELLIPSE, APERTURE={0.076,0.01915};')
        self.madx.input('mdh.12007, APERTYPE=RECTANGLE, APERTURE={0.076,0.01765};')
        self.madx.input('bph.12008, APERTYPE=RECTANGLE, APERTURE={0.0725,0.0179};')
        self.madx.input('mdva.11904, APERTYPE=ELLIPSE, APERTURE={0.071,0.071};')


        
    #Method to return Twisses 
    def return_twisses(self): 
        #Attempt Twiss commands for the injected and circulating beam , restart MADX if it crashes 
        try:      
            #Injected beam
            self.madx.use(sequence='short_injection_and_some_sps')
            self.twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                    bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                                   dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                    dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                    x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                    px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
                    
            #Circulating beam
            self.madx.use(sequence="tt2tt10sps")
            self.twiss_c = self.madx.twiss(betx=self.initbeta0['betx'], alfx=self.initbeta0['alfx'],
                                bety=self.initbeta0['bety'], alfy=self.initbeta0['alfy'], 
                               dx=self.initbeta0['dx'], dpx=self.initbeta0['dpx'],
                                dy = self.initbeta0['dy'], dpy=self.initbeta0['dpy'],
                                x=self.initbeta0['x'], y=self.initbeta0['y'],
                                px=self.initbeta0['px'], py=self.initbeta0['py']).dframe()
    
        #Restart MADX if it crashes
        except RuntimeError:
            print("Resetting MADX...") 
            with open('tempfile', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()
        
        
    #Method to plot the oscillations 
    def plot_oscillations(self, fig): 
        fig.suptitle('SPS oscillations - injected and circulating beam',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        #Define some parameters for the circulating beam, and plot it on the same axis as the injected 
        if self.optics == 'sftpro':
            nx = 6
        else:
            nx = 5
        #Plot the injected beam:
        acc_phy_lib_elias.plot_envelope(self.twiss, self.sige, self.ex, self.ey, ax, nx=nx)
        #Align the injected and circulating beam longitudinally, then plot it on the same axis
        ds = self.twiss.loc['qf.12010'].s - self.twiss_c.loc['qf.12010'].s
        self.twiss_c['s'] = self.twiss_c['s'].add(ds)   
        acc_phy_lib_elias.plot_envelope(self.twiss_c, self.sige, self.ex, self.ey, ax, nx=nx, hcolor="c")       
        plt.ylim(-0.1, 0.1)
        plt.xlim(30, 550)
        
        
    #If context manager has been used, print the lines of the temporary error file 
    def print_madx_error(self):
        with open('tempfile', 'r') as f:
            lines = f.readlines()
            for ele in lines:
                if '+=+=+= fatal' in ele:
                    print('{}'.format(ele))
