"""
Script to load trained RL models for the MKP delays, and compare them all on different initial conditions 
to create performance plots 
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time 
import gym
import copy

from stable_baselines3 import PPO, A2C # DQN coming soon
from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
from mkpdelays_env_RL import MKPOptEnv

# Initiate and load model 
n_steps = 50000
dtc = 250
model_str = "plots_and_data/model _{}_steps_{}_ns_dtc".format(n_steps, dtc)
save_plot = True
load_data = True  # if loading pickled reward data
plot_neg_reward_log = False # whether to plot negative rewards, then we have to flip the cumulative distributions 
n_steps_IC = 100  # how many different initial conditions to test the models with 
n_steps_test = 5 # how many steps per set of IC the models can take 

# Initiate environment
env =  MKPOptEnv(dtc=dtc)   # also include batch spacing time separation

# ------------------- Load models from training --------------------------------------
model_TD3 = TD3.load("{}_TD3_with_noise_sigma_0dot02".format(model_str))
model_A2C = A2C.load("{}_A2C".format(model_str))
model_PPO = PPO.load("{}_PPO".format(model_str))

if not load_data:
    # ---------------------- Test the trained agent ------------------------
    rewards = {'TD3':[], 'A2C':[], 'PPO':[]}
    
    for IC in range(n_steps_IC):
        print("\nIC number {}".format(IC+1))
        
        env.reset_MKP_delays()  
        obs = env.reset()  # first reset and observe where initial real action is 
       
        # copy environment and first observation to each model --> use deepcopy for independent objects 
        obs_TD3, obs_A2C, obs_PPO = copy.deepcopy(obs), copy.deepcopy(obs), copy.deepcopy(obs)
        env_TD3, env_A2C, env_PPO  = env, env, env 
    
        for step in range(n_steps_test):
            print("Step {}".format(step + 1))
            
            # ----------- Test all the models --------------------------- 
            action_TD3, _ = model_TD3.predict(obs_TD3, deterministic=True)
            obs_TD3, reward_TD3, done_TD3, info_TD3 = env_TD3.step(action_TD3)
            
            action_A2C, _ = model_A2C.predict(obs_A2C, deterministic=True)
            obs_A2C, reward_A2C, done_A2C, info_A2C = env_A2C.step(action_A2C)
            
            action_PPO, _ = model_PPO.predict(obs_PPO, deterministic=True)
            obs_PPO, reward_PPO, done_PPO, info_PPO = env_PPO.step(action_PPO)
            
          
        print("Rewards: TD3={:.3e}, A2C={:.3e}, PPO={:.3e}\n".format(reward_TD3, reward_A2C, reward_PPO))
        rewards['TD3'].append(reward_TD3)
        rewards['A2C'].append(reward_A2C)
        rewards['PPO'].append(reward_PPO)
        
        del env_TD3, env_A2C, env_PPO, obs_TD3, obs_A2C, obs_PPO
    
    
    # Save this data to dumped file
    pickle_str = "plots_and_data/rewards_{}_ICs_sweeper_performance_plots_{}_ns_dtc.pickle".format(n_steps_IC, dtc)
    filehandler = open(pickle_str,"wb")
    pickle.dump(rewards, filehandler)
    filehandler.close()
else:
    # To unpickle this data
    with open(pickle_str, "rb") as handle:
        rewards = pickle.load(handle)

# Make histograms for performance plots (cumsums)
# Evaluate the histograms
values_TD3, base_TD3 = np.histogram(rewards['TD3'], bins=50)
values_A2C, base_A2C = np.histogram(rewards['A2C'], bins=50)
values_PPO, base_PPO = np.histogram(rewards['PPO'], bins=50)

# Evaluate the cumulative distributions
cumulative_TD3 = np.cumsum(values_TD3/n_steps_IC)
cumulative_A2C = np.cumsum(values_A2C/n_steps_IC)
cumulative_PPO = np.cumsum(values_PPO/n_steps_IC)

# Plot the different cumulative distributions
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(13, 7))
if plot_neg_reward_log:
    ax.plot(np.abs(base_TD3[:-1]), np.flip(cumulative_TD3), c='blue', label='TD3', linewidth=4)
    ax.plot(np.abs(base_A2C[:-1]), np.flip(cumulative_A2C), c='green', label='A2C', linewidth=4)
    ax.plot(np.abs(base_PPO[:-1]), np.flip(cumulative_PPO), c='red', label='PPO', linewidth=4)
    ax.set_xscale('log')
    ax.set_xlabel("Negative Reward")
else:
    ax.plot(base_TD3[:-1], cumulative_TD3, c='blue', label='TD3', linewidth=4)
    ax.plot(base_A2C[:-1], cumulative_A2C, c='green', label='A2C', linewidth=4)
    ax.plot(base_PPO[:-1], cumulative_PPO, c='red', label='PPO', linewidth=4)
    ax.set_xlabel("Reward")
ax.legend(loc='upper left', prop={'size': 19})
ax.yaxis.label.set_size(24)
ax.xaxis.label.set_size(24)
ax.set_ylabel("Fraction")
ax.tick_params(axis='both',labelsize=18)
fig.tight_layout()
#ax.set_xlim([0.46, 0.74])

if save_plot:
    fig.savefig(
            "plots_and_data/Performance_plots_{}_ICs_{}_ns_dtc.png".format(n_steps_IC, dtc),
            dpi=250,
        )