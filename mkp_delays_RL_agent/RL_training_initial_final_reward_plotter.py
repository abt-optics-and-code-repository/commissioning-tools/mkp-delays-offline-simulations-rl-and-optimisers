"""
Plotter to compare reward at the start and at the end of the episode
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
import pandas as pd


# Initiate and load model 
save_plot = False
algorithm = 'A2C'
n_steps = 50000
dtc = 250

# Prepare data strings for the different training runs  
save_str = "plots_and_data/initial_final_rewards_{}_ns_dtc_{}.png".format(dtc, algorithm)
data_str = "plots_and_data/episode_data_50000_steps_250_ns_dtc_A2C.pickle"
   
episode_reward_start = []
episode_reward_end = []
episode_len = []

#Unpickle the data
with open(data_str, "rb") as handle:
    episode_data = pickle.load(handle)

for ele in episode_data["all_rewards"]:
    episode_reward_start.append(ele[0])
    episode_reward_end.append(ele[-1])
    episode_len.append(len(ele))

# Plot initial and final reward per episode
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(29)
ax.xaxis.label.set_size(29)
plt.xticks(fontsize=22) 
plt.yticks(fontsize=22)
ax.plot(episode_reward_start, label="Initial reward", linewidth=2)
ax.plot(episode_reward_end, label="Final reward", linewidth=2)
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
ax.legend(prop={'size': 22}) 
fig1.tight_layout()
if save_plot:
    fig1.savefig(save_str, dpi=250)
    
    
# Plot episode length for all trainings
fig2 = plt.figure(figsize=(10,7))
ax2 = fig2.add_subplot(1, 1, 1)  # create an axes object in the figure
ax2.yaxis.label.set_size(29)
ax2.xaxis.label.set_size(29)
plt.xticks(fontsize=22) 
plt.yticks(fontsize=22)
ax2.plot(episode_len, linewidth=2, color='m')
ax2.set_xlabel("Training episode")
ax2.set_ylabel("Steps per episode")
fig2.tight_layout()
if save_plot:
    fig2.savefig("plots_and_data/steps_per_episode_{}_ns_dtc_{}.png".format(dtc, algorithm), dpi=250)

